from django.http import JsonResponse
from django.shortcuts import render
import this
# Create your views here.
import base64

def up_image(request):
    data = request.POST
    image= data.get("img")
    # todo 传到后台中的base64头部有 data:image/jpeg;base64, 这样的base64放在前端可以直接使用，但是python中不可以
    # todo 我们需要将data:image/jpeg;base64,这段删除掉，使用split(',')
    image = image.split(",")[1]
    # todo ，使用base64解编码
    img = base64.b64decode(image)
    # todo 保存
    with open("qqqqqqqqqq.png",'wb') as f:
        f.write(img)
    return JsonResponse("success",safe=False)