# -*- coding: utf-8 -*-
# data : 2018/8/28/028


from django.conf.urls import url
from app import consumers

# web_client =
websocket_urlpatterns = [
url(r'^ws/user/(?P<role_pk>[^/]+)/$', consumers.ServiceConsumer),
]