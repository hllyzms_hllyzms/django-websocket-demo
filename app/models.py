from django.db import models

# Create your models here.
from django.contrib.auth.models import User


class Note(models.Model):
	user = models.ForeignKey(User, blank=True, null=True)
	
	title = models.CharField(max_length=200)
	body = models.TextField()
	create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
	update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间')
	
	def __str__(self):
		return self.title
	
	class Meta:
		db_table = "note"


class Table(models.Model):
	TimeSeconds = models.IntegerField()
	TagID = models.CharField(max_length=20)
	num = models.FloatField()
	clas = models.IntegerField(default=1)
	
	class Meta:
		db_table = "table1"