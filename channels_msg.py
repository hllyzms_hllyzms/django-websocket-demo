# -*- coding: utf-8 -*-
# @Time    : 2019/3/19 9:56
# import time
# import os
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "channels_demo.settings")
#
# from channels.layers import get_channel_layer
# from asgiref.sync import async_to_sync
# channel_layer = get_channel_layer()
# # todo channel_name 是保存时 self.room_group_name 的名字
#
# def send_channel_msg(channel_name, msg):
#     async_to_sync(channel_layer.group_send)(channel_name,{'type': 'user_message','text': msg})
#
#
# if __name__ == '__main__':
#     num = 1
#     while True:
#         send_channel_msg("user_5",{"mdms":"你好_%s"%num})
#         print(time.time())
#         num +=1
#         time.sleep(5)



import time
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "channels_demo.settings")

from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
channel_layer = get_channel_layer()
# todo channel_name 是保存时 self.room_group_name 的名字


def send_channel_msg(channel_name, msg):
    async_to_sync(channel_layer.group_send)(channel_name,{'type': 'user_message','text': msg})

def screen_channel_msg(channel_name, msg):
    async_to_sync(channel_layer.group_send)(channel_name, {'type': 'user_message','text': msg})
    
    
    
if __name__ == '__main__':
    send_channel_msg("user_5", {"type": "affiche","info":{
    "id": 1,
    "classify": "公告",
    "content": "上班信息",
    "create_time": "2019-05-16"
}})
    print(time.time())

